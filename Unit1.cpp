//---------------------------------------------------------------------------#pragma hdrstop
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <vcl.h>
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
: TForm(Owner)
{
}
//---Global------------------------------------------------------------------
int A[10000],a,j,p,B[10000],C[40][20],i,x,d,l=20;
//---------------------------------------------------------------------------
//�������� �������� �������
void swap (int *a, int *b)
{
    int temp=*a; *a=*b; *b=temp;
}
//������ � ��������� ������ ��� ������������� ��������� ����������
void shag (int e, int *r, int v[40][20])
{
    int q;
    if (e<16) {
        for (q=0;q<e;q++){
            v[d][q]=r[q+1];
        }
        d++;
    }
}
//�������
void q_sort (int l,int u, int *x)
{
    int m, t;
    if (l<u){
        t=x[l]; m=l;
        for (i=l; i<=u; i++)
        if (x[i]<t){
            m++; swap (&x[m],&x[i]);
        }
        swap (&x[l], &x[m]);
        q_sort (l, m-1, x);
        q_sort (m+1, u, x);
    }
}
//��������
void min (int b, int *y)
{
    int min;
    x=1;
    for (i=1; i<=b; i++){
        shag (b, y, C);
        min=99999999;
        for (j=x; j<=b; j++){
            if (y[j]<min) {
                min=y[j];
                swap (&y[j],&y[i]);
            }
        }
        x++;
    }
}
//�������
void puz(int b, int *y)
{
    int c=1;
    while (c!=0) {
        c=1;
        shag (b, y, C);
        for (j=1; j<b; j++) {
            if (y[j]>y[j+1]) {
                swap (&y[j], &y[j+1]); c++;
            }
        }
        c--;
    }
}
//�����
void shell(int b, int *y)
{
    int d; bool k;
    d=b/2;
    while (d>0) {
        k=true;
        while (k) {
            k=false;
            for (i=1; i<=b-d; i++) {
                if (y[i]>y[i+d]) {
                    shag (b, y, C);
                    swap (&y[i], &y[i+d]);
                    k=true;
                }
            }
        }
        d--;
    }
    shag (b, y, C);
}
//��������� ������� �������
void predel (int b, int *y, int c, int f)
{
    int q;
    if (c<0&&f<0) q=1;
    if (c<=0&&f>=0) q=2;
    if (c>=0&&f>0) q=3;
    switch (q) {
        case 1: for (i=1; i<=b; i++){
            y[i]=random(fabs(f)-fabs(c)-1)-fabs(c);
        }
        break;
        case 2: for (i=1; i<=b; i++){
            y[i]=random(f+fabs(c)+1)+c;
        }
        break;
        case 3: for (i=1; i<=b; i++){
            y[i]=random(fabs(f)-fabs(c)+1)+fabs(c);
        }
        break;
    }
}
//���� ��������� �� �����
void fromfile (AnsiString l, int b, int *y)
{
    const int SIZE = 100;
    char *ch = new char[SIZE];
    ch=l.c_str();
    char *x[SIZE] = {
        0
    }
    ;
    char *p = strtok(ch, ",");
    i = 0;
    for (j=1; j<=b; j++)
    {
        x[i++] = p;
        p = strtok(NULL, ",");
    }
    i = 0;
    j = 1;
    while(x[i]!= 0){
        y[j]=atoi(x[i]);
        i++;j++;
    }
}
void razmer (int p, int o)
{
    if (o==0) {
        o=1;
    }
    if (p==0) {
        p=1;
    }
    while ((p!=0)&&(o!=0)){
        p/=10;
        o/=10;
        l+=5;
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ComboBox1Change(TObject *Sender)
{
    int k;
    k=ComboBox1->ItemIndex;
    Series1->Clear();
    StringGrid1->ColCount=a;
    StringGrid1->Width=321;
    if (a>15) {
        ShowMessage("����� ��������� ��������� 15, ���������� ������������ ��� ������������");
    }
    //��������� ���� ���������
    if (k==1) {
        Label7->Visible=true;
        Label6->Visible=true;
        Label5->Visible=true;
        Button10->Visible=true;
        Edit1->Visible=true;
        Edit2->Visible=true;
    }
    //������ ���� ���������
    if (k==2) {
        Button4->Visible=true;
        Label2->Visible=false;
        ComboBox1->Visible=false;
        Label4->Visible=false;
        ComboBox2->Visible=false;
        Label3->Visible=true;
        StringGrid1->Visible=true;
        ShowMessage ("������� �������� ������� � ������� ����");
    }
    //���� ��������� �� �����
    if (k==0) {
        int max=-99999999;
        ShowMessage("������ � ����� ������ ���� ������� � ������� ����� �������");
        OpenDialog1->Execute();
        Memo1->Lines->LoadFromFile(OpenDialog1->FileName);
        AnsiString s;
        s=Memo1->Text;
        fromfile (s,a,A);
        for (i=1; i<=a; i++){
            StringGrid1->Cells[i-1][0]=A[i];
            if (A[i]>max) {
                max=A[i];
            }
        }
        if (a<=15) {
            for (i=1; i<=a; i++){
                Series1->Add(A[i]);
            }
        }
        razmer (max,max);
        StringGrid1->DefaultColWidth=l;
        if (a*l>321) {
            StringGrid1->Height=41;
        }
        //����� �������
        Label2->Visible=false;
        ComboBox1->Visible=false;
        Label4->Visible=true;
        ComboBox2->Visible=true;
        Label3->Visible=true;
        StringGrid1->Visible=true;
        if (a<=15) {
            Chart1->Visible=true;
            Form1->Height=390;
        }
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
    Series1->Clear();
    Button2->Visible=false;
    if (p!=3) {
        if (a<16) {
            Button8->Visible=true;
        }
    }
    //����� ���������
    if (p==1) {
        min (a, A);
        for (i=1; i<=a; i++){
            StringGrid1->Cells[i-1][0]=A[i];
        }
        if (a<=15) {
            for (i=1; i<=a; i++){
                Series1->Add(A[i]);
            }
        }
    }
    //����� ��������
    if (p==0) {
        puz (a, A);
        for (i=1; i<=a; i++){
            StringGrid1->Cells[i-1][0]=A[i];
        }
        if (a<=15) {
            for (i=1; i<=a; i++){
                Series1->Add(A[i]);
            }
        }
    }
    //����� �����
    if (p==2) {
        shell (a, A);
        for (i=1; i<=a; i++){
            StringGrid1->Cells[i-1][0]=A[i];
        }
        if (a<=15) {
            for (i=1; i<=a; i++){
                Series1->Add(A[i]);
            }
        }
    }
    //������� ����������
    if (p==3){
        q_sort (1, a, A);
        for (i=1; i<=a; i++){
            StringGrid1->Cells[i-1][0]=A[i];
        }
        if (a<=15) {
            for (i=1; i<=a; i++){
                Series1->Add(A[i]);
            }
        }
    }
    if (a<=15) {
        for (i=1;i<=a;i++) {
            B[i]=A[i];
        }
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
    Close();                        //�����
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ComboBox2Change(TObject *Sender)
{
    p=ComboBox2->ItemIndex;  //������ ����������
    ComboBox2->Visible=false;
    Label4->Visible=false;
    Button2->Visible=true;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button5Click(TObject *Sender)
{
    //�������
    Button3->Visible=true;
    Memo1->Visible=true;
    Chart1->Visible=false;
    Memo1->Height=370;
    Memo1->Width=573;
    Memo1->Lines->LoadFromFile("1.txt");
    Form1->Height=550;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
    Button3->Visible=false;                              //��������� �� �������
    Memo1->Visible=false;
    if (a<16) {
        if (Label3->Visible==true){
            Chart1->Visible=true;
            Form1->Height=390;
        }
        else Form1->Height=116;
    }
    if (a>15) {
        Form1->Height=116;
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)     //������ ����
{
    int max=-9999999;
    Series1->Clear();
    for (i=1; i<=a; i++){
        A[i]=StrToInt(StringGrid1->Cells[i-1][0]);
        if (A[i]>max) {
            max=A[i];
        }
    }
    razmer (max,max);
    StringGrid1->DefaultColWidth=l;
    if (a*l>321) {
        StringGrid1->Height=41;
    }
    if (a<=15) {
        for (i=1; i<=a; i++){
            Series1->Add(A[i]);
        }
        Chart1->Visible=true;
        Form1->Height=390;
    }
    //����� �������
    Button4->Visible=false;
    Label4->Visible=true;
    ComboBox2->Visible=true;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button6Click(TObject *Sender)
{
    x++;                                                 //�������� ������
    Series1->Clear();
    for (j=0; j<a; j++){
        Series1->Add(C[x][j]);
        if (C[x][j]==B[j+1]) {
            i++;
        }
    }
    if (x>0) {
        Button9->Visible=true;
    }
    if (i==a) {
        Button6->Visible=false; ShowMessage ("������ ������������");
    }
    i=0;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button8Click(TObject *Sender)
{
    //��������
    Button8->Visible=false;
    Button6->Visible=true;
    StringGrid1->Top=8;
    Label3->Top=8;
    Series1->Clear();
    x=0;
    i=0;
    for (j=0; j<a; j++){
        Series1->Add(C[x][j]);
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button7Click(TObject *Sender)
{
    //�����
    a=StrToInt(CSpinEdit1->Text);
    if (a<2) {
        ShowMessage ("����� ��������� ������ ���� ������ 1");
    }
    else {
        Label1->Visible=false;
        CSpinEdit1->Visible=false;
        Button7->Visible=false;
        Label2->Visible=true;
        ComboBox1->Visible=true;
    }
    if (a>15) {
        StringGrid1->Height=41;
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button9Click(TObject *Sender)      //�������� �����
{
    x--;
    Series1->Clear();
    for (j=0; j<a; j++){
        Series1->Add(C[x][j]);
    }
    if (x==0) {
        Button9->Visible=false;
    }
    if (x<a-1) {
        Button6->Visible=true;
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button10Click(TObject *Sender)
{
    //���� �������� ��������
    d=StrToInt(Edit1->Text);
    x=StrToInt(Edit2->Text);
    if (d<x) {
        razmer (d,x);
        StringGrid1->DefaultColWidth=l;
        if (a*l>321) {
            StringGrid1->Height=41;
        }
        //����� �������
        Label2->Visible=false;
        ComboBox1->Visible=false;
        Label4->Visible=true;
        ComboBox2->Visible=true;
        Label7->Visible=false;
        Label6->Visible=false;
        Label5->Visible=false;
        Button10->Visible=false;
        Edit1->Visible=false;
        Edit2->Visible=false;
        Label3->Visible=true;
        StringGrid1->Visible=true;
        predel (a,A,d,x);
        for (i=1; i<=a; i++)
        {
            StringGrid1->Cells[i-1][0]=A[i];
        }
        if (a<=15) {
            Chart1->Visible=true;
            Form1->Height=390;
            for (i=1; i<=a; i++)
            {
                Series1->Add(A[i]);
            }
        }
    }
    else ShowMessage ("����� ������ ������ ���� ������ �������");
    d=0;
}
//---------------------------------------------------------------------------
