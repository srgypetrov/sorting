object Form1: TForm1
  Left = 353
  Top = 224
  Width = 581
  Height = 114
  Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1082#1072' '#1084#1072#1089#1089#1080#1074#1086#1074
  Color = clActiveCaption
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 88
    Top = 8
    Width = 246
    Height = 20
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1101#1083#1077#1084#1077#1085#1090#1086#1074' '#1084#1072#1089#1089#1080#1074#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 88
    Top = 8
    Width = 278
    Height = 20
    Caption = #1057#1087#1086#1089#1086#1073' '#1079#1072#1076#1072#1085#1080#1103' '#1101#1083#1077#1084#1077#1085#1090#1086#1074' '#1084#1072#1089#1089#1080#1074#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label3: TLabel
    Left = 88
    Top = 40
    Width = 147
    Height = 20
    Caption = #1069#1083#1077#1084#1077#1085#1090#1099' '#1084#1072#1089#1089#1080#1074#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label4: TLabel
    Left = 88
    Top = 8
    Width = 206
    Height = 20
    Caption = #1042#1099#1073#1086#1088' '#1089#1087#1086#1089#1086#1073#1072' '#1089#1086#1088#1090#1080#1088#1086#1074#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label5: TLabel
    Left = 88
    Top = 32
    Width = 234
    Height = 20
    Caption = #1055#1088#1077#1076#1077#1083#1099' '#1079#1085#1072#1095#1077#1085#1080#1081' '#1101#1083#1077#1084#1077#1085#1090#1086#1074
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label6: TLabel
    Left = 88
    Top = 56
    Width = 21
    Height = 20
    Caption = #1054#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label7: TLabel
    Left = 192
    Top = 56
    Width = 21
    Height = 20
    Caption = #1044#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Chart1: TChart
    Left = 8
    Top = 72
    Width = 561
    Height = 281
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Brush.Color = clWhite
    Title.Brush.Style = bsClear
    Title.Color = clWhite
    Title.Font.Charset = DEFAULT_CHARSET
    Title.Font.Color = clWhite
    Title.Font.Height = -13
    Title.Font.Name = 'Arial'
    Title.Font.Style = []
    Title.Frame.Color = -1
    Title.Text.Strings = (
      #1057#1090#1086#1083#1073#1080#1082#1086#1074#1072#1103' '#1076#1080#1072#1075#1088#1072#1084#1084#1072)
    Legend.ColorWidth = 20
    Legend.Inverted = True
    Legend.TextStyle = ltsLeftPercent
    Legend.Visible = False
    BevelOuter = bvNone
    Color = clActiveCaption
    TabOrder = 0
    Visible = False
    object Series1: TBarSeries
      ColorEachPoint = True
      Marks.ArrowLength = 20
      Marks.Style = smsValue
      Marks.Visible = True
      SeriesColor = clRed
      VertAxis = aRightAxis
      BarStyle = bsRectGradient
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Bar'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
  object Button5: TButton
    Left = 8
    Top = 8
    Width = 73
    Height = 25
    Caption = #1057#1087#1088#1072#1074#1082#1072
    TabOrder = 1
    OnClick = Button5Click
  end
  object ComboBox1: TComboBox
    Left = 376
    Top = 8
    Width = 193
    Height = 21
    DropDownCount = 4
    ItemHeight = 13
    TabOrder = 2
    Text = #1053#1077' '#1074#1099#1073#1088#1072#1085#1086
    Visible = False
    OnChange = ComboBox1Change
    Items.Strings = (
      #1048#1079' '#1092#1072#1081#1083#1072
      #1057#1083#1091#1095#1072#1081#1085#1086
      #1056#1091#1095#1085#1086#1081' '#1074#1074#1086#1076)
  end
  object CSpinEdit1: TCSpinEdit
    Left = 352
    Top = 8
    Width = 217
    Height = 22
    TabOrder = 3
  end
  object ComboBox2: TComboBox
    Left = 304
    Top = 8
    Width = 265
    Height = 21
    DropDownCount = 5
    ItemHeight = 13
    TabOrder = 4
    Text = #1053#1077' '#1074#1099#1073#1088#1072#1085#1086
    Visible = False
    OnChange = ComboBox2Change
    Items.Strings = (
      #1052#1077#1090#1086#1076' '#1087#1091#1079#1099#1088#1100#1082#1072
      #1052#1077#1090#1086#1076' '#1084#1080#1085#1080#1084#1091#1084#1086#1074
      #1057#1086#1088#1090#1080#1088#1086#1074#1082#1072' '#1064#1077#1083#1083#1072
      #1041#1099#1089#1090#1088#1072#1103' '#1089#1086#1088#1090#1080#1088#1086#1074#1082#1072)
  end
  object Button1: TButton
    Left = 8
    Top = 40
    Width = 73
    Height = 25
    Caption = #1042#1099#1093#1086#1076
    TabOrder = 5
    OnClick = Button1Click
  end
  object StringGrid1: TStringGrid
    Left = 248
    Top = 40
    Width = 321
    Height = 25
    ColCount = 1
    DefaultColWidth = 20
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    ScrollBars = ssHorizontal
    TabOrder = 6
    Visible = False
    RowHeights = (
      24)
  end
  object Button2: TButton
    Left = 88
    Top = 8
    Width = 73
    Height = 25
    Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100
    TabOrder = 7
    Visible = False
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 248
    Top = 456
    Width = 75
    Height = 25
    Caption = #1042#1077#1088#1085#1091#1090#1100#1089#1103
    TabOrder = 8
    Visible = False
    OnClick = Button3Click
  end
  object Memo1: TMemo
    Left = 0
    Top = 80
    Width = 569
    Height = 145
    BorderStyle = bsNone
    Color = clActiveCaption
    ParentShowHint = False
    ReadOnly = True
    ShowHint = False
    TabOrder = 9
    Visible = False
  end
  object Button4: TButton
    Left = 88
    Top = 8
    Width = 75
    Height = 25
    Caption = #1042#1074#1086#1076
    TabOrder = 10
    Visible = False
    OnClick = Button4Click
  end
  object Button6: TButton
    Left = 496
    Top = 52
    Width = 75
    Height = 25
    Caption = #1042#1087#1077#1088#1077#1076
    TabOrder = 11
    Visible = False
    OnClick = Button6Click
  end
  object Button8: TButton
    Left = 88
    Top = 8
    Width = 75
    Height = 25
    Caption = #1055#1086#1096#1072#1075#1086#1074#1086
    TabOrder = 12
    Visible = False
    OnClick = Button8Click
  end
  object Button7: TButton
    Left = 88
    Top = 40
    Width = 73
    Height = 25
    Caption = #1044#1072#1083#1077#1077
    TabOrder = 13
    OnClick = Button7Click
  end
  object Button9: TButton
    Left = 416
    Top = 52
    Width = 75
    Height = 25
    Caption = #1053#1072#1079#1072#1076
    TabOrder = 14
    Visible = False
    OnClick = Button9Click
  end
  object Edit1: TEdit
    Left = 120
    Top = 56
    Width = 65
    Height = 21
    TabOrder = 15
    Visible = False
  end
  object Edit2: TEdit
    Left = 216
    Top = 56
    Width = 65
    Height = 21
    TabOrder = 16
    Visible = False
  end
  object Button10: TButton
    Left = 288
    Top = 55
    Width = 75
    Height = 25
    Caption = #1042#1074#1086#1076
    TabOrder = 17
    Visible = False
    OnClick = Button10Click
  end
  object OpenDialog1: TOpenDialog
    Filter = '*txt|*txt'
    Left = 8
    Top = 72
  end
end
